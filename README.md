# sce_project_tienchilin

Simple PID control system simulation with data-driven models

## Installation

```bash
$ pip install sce_project_tienchilin
```

## Usage

- There are two main parts of this project. The first part is to derive data-driven models from the dataset, while the second part is to conduct PID system simulation from those data-driven models with autotuned/manually determined PID parameters. Detailed steps are shown in the usage example.

## Contributing

Interested in contributing? Check out the contributing guidelines. Please note that this project is released with a Code of Conduct. By contributing to this project, you agree to abide by its terms.

## License

`sce_project_tienchilin` was created by Tien-Chi Lin. It is licensed under the terms of the MIT license.

## Credits

`sce_project_tienchilin` was created with [`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/) and the `py-pkgs-cookiecutter` [template](https://github.com/py-pkgs/py-pkgs-cookiecutter).
